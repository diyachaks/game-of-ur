import random
import numpy as np

class Player(object):
    """docstring for Player
    AI chooses which piece to play here
    Available data:
      piecces: the set of movable pieces for AI player
      self.board: Game board with complete state"""

    def __init__(self):
        
        super(Player, self).__init__()
    
    def min_path(self,pieces ,step, board):
        import numpy as np
        path = board.p1Path
        path_left=[]
        for i in pieces :
            path_left.append(len(path)-(board.piecesPosition[i]+ step ))
        return pieces[np.argmin(path_left)]
    def forward_prop(self,pieces ,step, board , step_prob):
        oppo_pieces = board.opponent_piece(pieces[0])
        fut_pos=dict()
        danger_prob = {k: [] for k in pieces}
        move_pieces=[]
        for i in pieces :
            (player, piece, pieces1, path) = board.idByPiece(i)
            
            fut_pos[i] = board.piecesPosition[i]+step
            (occupied, pidO, playerO)=board.isOccupied(fut_pos[i])
            
            if not occupied and path[fut_pos[i]] not in board.COMMON_PATH and path[board.piecesPosition[i]] not in board.COMMON_PATH:
                move_pieces.append(i)
            elif not occupied :
                for j in oppo_pieces :
                    if fut_pos[i]- board.piecesPosition[j]<=4 and fut_pos[i]- board.piecesPosition[j]>0 :
                        danger_prob[i].append(step_prob[fut_pos[i]- board.piecesPosition[j]])
                        
                if len(danger_prob[i])==0 :
                    move_pieces.append(i)
                else:
                    danger_prob[i] = np.min(danger_prob[i])
        minval = min(danger_prob.values())
        move_pieces.extend([k for k, v in danger_prob.items() if v==minval])
        
        return move_pieces
                
    def choose(self,iplayer, pieces, step, board , step_prob):
        cur_pos=dict()
        
        pieces=[iplayer+x for x in pieces]
        oppo_pieces = board.opponent_piece(pieces[0])
        danger_pieces=[]
        kill_pieces=[]
        rosette_pieces=[]
        safe_pieces=[]
        start_pieces=[]
        normal_pieces=[]
        complete_piece=[]
        danger_index = dict()
        if len(pieces) ==1:
            return pieces[0][1]
        pref=0
        for i in pieces :
            (player, piece, pieces1, path) = board.idByPiece(i)
            cur_pos[i] = board.piecesPosition[i]
            danger_score=[]
            for j in oppo_pieces :
                if cur_pos[i]- board.piecesPosition[j]<=4 and cur_pos[i]- board.piecesPosition[j]>0 and (path[cur_pos[i]] not in board.DUAL_CHANCE) and (path[cur_pos[i]]  in board.COMMON_PATH ) :
                    if i not in danger_pieces:
                        danger_pieces.append(i)
                    danger_score.append(step_prob[cur_pos[i]- board.piecesPosition[j]])
            if len(danger_score) !=0:
                danger_index[i]= max(danger_score)
            else:
                danger_index[i]= 0
            
            if cur_pos[i] + step == len(path) :
                complete_piece.append(i)
                if path[cur_pos[i]] in board.COMMON_PATH :
                    pref+=1
                continue
            (occupied, pidO, playerO)=board.isOccupied(cur_pos[i]+step)
            
            if occupied == True :
                if player!=playerO and (path[cur_pos[i] + step] not in board.COMMON_PATH) and (path[cur_pos[i]] in board.COMMON_PATH) and (i in danger_pieces):
                        
                    safe_pieces.append(i)
                if player!=playerO and (path[cur_pos[i] + step] not in board.DUAL_CHANCE) and (path[cur_pos[i] + step]  in board.COMMON_PATH):
                        
                    kill_pieces.append(i)
                if player!=playerO and (path[cur_pos[i] + step] in board.DUAL_CHANCE) and (path[cur_pos[i] + step] not in board.COMMON_PATH):
                        
                    rosette_pieces.append(i)
                  
                if cur_pos[i]==-1 and player!=playerO:
                    start_pieces.append(i)
                
                    
            else :
                if  ((path[cur_pos[i] + step] not in board.COMMON_PATH)  or (path[cur_pos[i] + step]  in board.DUAL_CHANCE)  )and (path[cur_pos[i]] in board.COMMON_PATH) and (i in danger_pieces):
                        
                    safe_pieces.append(i)
                if (path[cur_pos[i] + step] in board.DUAL_CHANCE):
                        
                    rosette_pieces.append(i)
                if cur_pos[i]==-1 :
                    start_pieces.append(i)
                
            if len(complete_piece)==0 and len(safe_pieces)==0 and len(kill_pieces)==0 and len(rosette_pieces)==0 and len(start_pieces)==0:
                    normal_pieces.append(i)
        
        
        for i in danger_pieces :
            (player, piece, pieces1, path) = board.idByPiece(i)
            
            fut_pos = board.piecesPosition[i]+step
            (occupied, pidO, playerO)=board.isOccupied(fut_pos)
            
            if  occupied and playerO == player:
                danger_pieces.remove(i)
                continue
            else:
                danger_prob = {k: [] for k in danger_pieces}
                for j in oppo_pieces :
                    if fut_pos- board.piecesPosition[j]<=4 and fut_pos- board.piecesPosition[j]>0 :
                        danger_prob[i].append(step_prob[fut_pos- board.piecesPosition[j]])
                    else:
                        danger_prob[i].append(0)
                if danger_index[i]< max(danger_prob[i]):
                    danger_pieces.remove(i)
                
        finished = 0 
        for  i in oppo_pieces : 
            (player, piece, pieces1, path) = board.idByPiece(i)
            if board.isPieceFinished(i, path):
                finished+=1
        
        
        piece= self.select_piece(cur_pos, complete_piece, safe_pieces, kill_pieces, rosette_pieces, danger_pieces, start_pieces, normal_pieces, pref , step,board ,path, step_prob , finished )
        return piece
            
    
    
    
    
    def select_piece(self, current_pos, complete_piece, safe_pieces, kill_pieces, rosette_pieces, danger_pieces, start_pieces, normal_pieces, preference , step,board ,path, step_prob , finished ):
        if finished >=4:
            if len(complete_piece)!=0 and preference==1 :
                piece=complete_piece[0]
                
                return piece[1]
            elif len(complete_piece)!=0 and len(kill_pieces)!=0:
                piece = self.min_path(kill_pieces,step,board)
                
                return piece[1]
            elif len(complete_piece)!=0 and len(rosette_pieces)!=0:
                piece = self.min_path(rosette_pieces,step,board)
                
                return piece[1]
            elif len(complete_piece)!=0 and len(safe_pieces)!=0:
                piece = self.min_path(safe_pieces,step,board)
                
                return piece[1]
            
        
            elif len(complete_piece)!=0 and  len(safe_pieces)==0 and len(kill_pieces)==0 and len(rosette_pieces)==0:
                piece=complete_piece[0]
                
                return piece[1]
            elif len(kill_pieces)!=0:
                piece = self.min_path(kill_pieces,step,board)
                
                return piece[1] 
            elif len(rosette_pieces)!=0:
                piece = self.min_path(rosette_pieces,step,board)
                
                return piece[1]
            elif  len(safe_pieces)!=0:
                piece = self.min_path(safe_pieces,step,board)
               
                return piece[1]
            
            
            elif len(danger_pieces)!=0:
                piece = self.min_path(danger_pieces,step,board)
                
                return piece[1]
            elif len(start_pieces)!=0:
                piece = random.choice(start_pieces)
                
                return piece[1]
        
            elif len(normal_pieces)!=0:
                normal_pieces = self.forward_prop(normal_pieces , step , board , step_prob)
                if len(normal_pieces)>1:
                    for i in normal_pieces :
                        if path[current_pos[i]] in board.DUAL_CHANCE and path[current_pos[i]] in board.COMMON_PATH:
                            normal_pieces.remove(i)
                piece = self.min_path(normal_pieces,step,board)
               
                return piece[1]
        
        if len(complete_piece)!=0 and preference==1 :
            piece=complete_piece[0]
            
            return piece[1]
        elif len(complete_piece)!=0 and len(safe_pieces)!=0:
            
            piece = self.min_path(safe_pieces,step,board)
                      
            return piece[1]
        elif len(complete_piece)!=0 and len(kill_pieces)!=0:
            piece = self.min_path(kill_pieces,step,board)
            
            return piece[1]
        elif len(complete_piece)!=0 and len(rosette_pieces)!=0:
            piece = self.min_path(rosette_pieces,step,board)
            
            return piece[1]
        elif len(complete_piece)!=0 and  len(safe_pieces)==0 and len(kill_pieces)==0 and len(rosette_pieces)==0:
            piece=complete_piece[0]
            
            return piece[1]
        elif len(safe_pieces)!=0:
            piece = self.min_path(safe_pieces,step,board)
            
            return piece[1]
        elif len(kill_pieces)!=0:
            piece = self.min_path(kill_pieces,step,board)
            
            return piece[1] 
        elif len(rosette_pieces)!=0:
            piece = self.min_path(rosette_pieces,step,board)
            
            return piece[1]
        elif len(danger_pieces)!=0:
            piece = self.min_path(danger_pieces,step,board)
            
            return piece[1]
        elif len(start_pieces)!=0:
            piece = random.choice(start_pieces)
            
            return piece[1]
        
        
        
        
        elif len(normal_pieces)!=0:
            normal_pieces = self.forward_prop(normal_pieces , step , board , step_prob)
            if len(normal_pieces)>1:
                for i in normal_pieces :
                    if path[current_pos[i]] in board.DUAL_CHANCE and path[current_pos[i]] in board.COMMON_PATH:
                        normal_pieces.remove(i)
            piece = self.min_path(normal_pieces,step,board)
            
            return piece[1]
        
        
        
    